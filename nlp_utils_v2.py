"""
    Python script containing modules that help pre-process text for use in
    deep learning
    Author: Bill Riedl
    Date: 20191007

    A few conventions:
    Padding tokens = PADDING_TOKEN and is index 0 in vecorization mappings
    Unknown tokens = UNKNOWN_TOKEN and is index 1 in vectorization mappings
"""
# imports
import numpy as np
from itertools import islice
from multiprocessing import Pool, cpu_count
import functools
import operator
import random
from collections import Counter

#####################################################################
# Class definitions
#####################################################################
class Error(Exception):
    """Error base class"""
    pass

class EmbedDimWrongError(Error):
    """Raised when expected embedding dimensions does not match the pretrained embedding file"""
    pass


#################################################################
# label functions
#################################################################
def getLabelsWorker(one_doc, label_key):
    return one_doc[label_key]

def createLabelToNumIds(documents, label_key='label', my_logger=None, zero_label=None):
    with Pool(cpu_count()) as p:
        labels = p.starmap(getLabelsWorker, [(doc, label_key) for doc in documents])
    labelSet = set(labels)

    label2Idx = {}
    if zero_label:
        if zero_label in labelSet:
            label2Idx[zero_label] = 0
            labelSet.discard(zero_label)
    # labels
    for label in labelSet:
        label2Idx[label] = len(label2Idx)
    # reverse the label index
    idx2Label = {v: k for k, v in label2Idx.items()}
    labelIdentities = np.identity(len(idx2Label), dtype='int32')

    return label2Idx, idx2Label, labelIdentities

def convertLabelToNumberWorker(one_doc, text_label_key, idx_label_key, label2Idx, labelIdentities):
    one_doc[idx_label_key] = labelIdentities[label2Idx.get(one_doc[text_label_key], 0)]
    return one_doc


def convertSeqLabelToNumberWorker(one_doc, text_label_key, idx_label_key, label2Idx):
    label_idx = []
    for text_label in one_doc[text_label_key]:
        label_idx.append(label2Idx.get(text_label, 0))
    one_doc[idx_label_key] = np.array(label_idx, dtype='int32')
    return one_doc


#################################################################
# token functions
#################################################################
def gettoksWorker(one_doc):
    return one_doc['toks']

def createTokToNumIds(documents, my_logger=None):
    """
    Take documents as input
    create text to number indexes for:
    1. words
    2. case
    3. characters
    4. labels
    """
    if my_logger:
        my_logger.info("Extracting unique toks and labels")
    with Pool(cpu_count()) as p:
        token_lists = p.map(gettoksWorker, documents)
    all_toks = functools.reduce(operator.iconcat, token_lists, [])
    tokCounts = Counter(all_toks)
    tokSet = set(all_toks)

    #######################
    # Word Embeddings - must be provided from some other source
    #######################
    tok2Idx = {}
    if my_logger:
        my_logger.info('No pre-trained embeddings.  Just creating word to idx')
    # We let the terms from the training set define the size of the vocabulary
    # add the unknown and padding toks - in case documents we want to predict values for have terms NOT in the training set
    tok2Idx["PADDING_TOKEN"] = len(tok2Idx)  # This will be index 0
    tok2Idx["UNKNOWN_TOKEN"] = len(tok2Idx)  # this will be index 1
    tok2Idx["SOS_TOKEN"] = len(tok2Idx)  # this will be index 2
    tok2Idx["EOS_TOKEN"] = len(tok2Idx)  # this will be index 3
    for tok in tokSet:
        tok2Idx[tok] = len(tok2Idx)

    # reverse lookup for tok2Idx
    idx2Tok = {v: k for k, v in tok2Idx.items()}
    # create a token identity matrix
    tokIdentities = np.identity(len(tok2Idx), dtype='int32')

    return tok2Idx, idx2Tok, tokCounts, tokIdentities

def loadPreTrainedEmbeddings(pretrainedWordEmbeddingsFilePath, embedDims, tok2Idx, my_logger=None):
    '''
    Assumes word embedding file is in the format:
    token -space- vector[0] -space- vector[1] -space- vector[2]
    Example:
    We -0.057268083 -0.044960648 0.04710306 0.049918618

    Note: this will set the initial weights for tokens in our vocbulary but NOT in the pre-trained embeddings
    to all zeros.  It is therefor reccomended that these be used to initialized a word embeddings layer but that
    the weights can be updated during training to help account for the terms without pre-trained embeddings

    :param pretrainedWordEmbeddingsFilePath:
    :param my_logger:
    :return:
    '''
    # pre-create a 2D matrix to contain the word embeddings
    tokEmbeddings = np.random.uniform(-0.25, 0.25, (len(tok2Idx), embedDims))

    if my_logger:
        my_logger.info('Building word embeddings list from pre-trained sources')
    # if pre-trained vectors exist, we limit our vocabulary to the terms for which we have vectors
    preTrainedWordVectors = open(pretrainedWordEmbeddingsFilePath, encoding="utf-8")

    # ALWAYS add padding and unknown tokens at positions 0 and 1 respectively:
    vector = np.zeros(embedDims)  # zero vector for 'PADDING' word
    tokEmbeddings[tok2Idx.get("PADDING_TOKEN",0)] = vector

    vector = np.random.uniform(-0.25, 0.25, embedDims)
    tokEmbeddings[tok2Idx.get("UNKNOWN_TOKEN",1)] = vector

    try:
        for line in preTrainedWordVectors:
            split = line.strip().split(" ")  # split[0] is the word, split[1] - split[N] is the vector
            tok = split[0]
            tok_idx = tok2Idx.get(tok, -1)

            vector = np.array([float(num) for num in split[1:]])
            if vector.shape[0] > embedDims:
                raise EmbedDimWrongError

            if tok_idx > 3:  # 3 based on the fact that 0,1,2,3 are research for unk, pad, eos and sos
                tokEmbeddings[tok_idx] = vector # word embedding vector

    except EmbedDimWrongError:
        if my_logger:
            my_logger.error("Embeddings file contains the wrong number of dimensions!")
            my_logger.error(f"Embedding file dims {vector.shape}, Requested {embedDims}")
        else:
            print("Embeddings file contains the wrong number of dimensions!")
            print(f"Embedding file dims {vector.shape}, Requested {embedDims}")

    return tokEmbeddings

def convertTokToNumbersWorker(one_doc, tok2Idx):
    # convert toks
    tok_idx = []
    toks = one_doc['toks']
    for one_tok in toks:
        tok_idx.append(tok2Idx.get(one_tok, 1))  # append index OR 1 for unknown token
    one_doc['toks_idx'] = np.array(tok_idx, dtype='int32')
    return one_doc


#################################################################
# Case functions
#################################################################
def createCaseToNumIds(my_logger=None):
    # mapping for token cases
    case2Idx = getCase2Idx()  # gets case dictionary from helper function
    idx2Case = {v: k for k, v in case2Idx.items()}
    # identity matrix used aka one hot encoding
    caseIdentities = np.identity(len(case2Idx), dtype='int32')
    return case2Idx, idx2Case, caseIdentities

def convertCaseToNumWorker(one_doc, case2Idx):
    # convert cases
    case_idx = []
    for case in one_doc['cases']:
        case_idx.append(case2Idx.get(case, 1))
    one_doc['cases_idx'] = np.array(case_idx, dtype='int32')
    return one_doc


def getCase2Idx():
    return {'PADDING_TOKEN': 0, 'UNKNOWN_TOKEN': 1,
            'allLower': 2, 'allUpper': 3,
            'initialUpper': 4, 'containsDigit': 5,
            'mainlyNumeric': 6, 'numeric': 7}


def getCasing(word):
    """
    For a word, return a casing mode that is most appropriate.  For example:
    BILL = allUpper
    Bill = initialUpper
    B1ll = containsDigit
    """

    numDigits = 0
    for char in word:
        if char.isdigit():
            numDigits += 1

    digitFraction = numDigits / float(len(word))

    if word.isdigit():  # Is a digit
        casing = 'numeric'
    elif digitFraction > 0.5:
        casing = 'mainlyNumeric'
    elif word.islower():  # All lower case
        casing = 'allLower'
    elif word.isupper():  # All upper case
        casing = 'allUpper'
    elif word[0].isupper():  # is a title, initial char upper, then all lower
        casing = 'initialUpper'
    elif numDigits > 0:
        casing = 'containsDigit'
    else:
        casing = 'UNKNOWN_TOKEN'

    return casing


#################################################################
# Character functions
#################################################################
def createCharToNumIds(my_logger=None):
    # dictionary of all possible characters
    char2Idx = getChar2Idx()
    idx2Char = {v: k for k, v in char2Idx.items()}
    charIdentities = np.identity(len(char2Idx), dtype='int32')
    return char2Idx, idx2Char, charIdentities


def getChar2Idx():
    """
    Creates an index from character to numeric index
    """
    char2Idx = {"PADDING_TOKEN": 0, "UNKNOWN_TOKEN": 1}
    for c in " 0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ\
             .,-_()[]{}!?:;#'\"/\\%$`&=*+@^~|<>":
        char2Idx[c] = len(char2Idx)
    return char2Idx

def convertCharToNumWorker(one_doc, char2Idx, char_pad_len):
    # convert chars - list of lists
    chars_idx = []
    for char_list in one_doc['chars']:
        char_list_idx = []
        for char in char_list:
            char_list_idx.append(char2Idx.get(char, 1))
        chars_idx.append(char_list_idx)
    one_doc['chars_idx'] = stack_pad(chars_idx, row_len=char_pad_len)

    # convert all_chars
    all_chars_idx = []
    for one_char in one_doc['all_chars']:
        all_chars_idx.append(char2Idx.get(one_char, 1))
    one_doc['all_chars_idx'] = all_chars_idx
    return one_doc

def addCaseAndCharInformationWorker(one_doc):
    '''
    Assume one_doc is a dictionary with key 'toks'
    add case and character information to one_doc dictionary
    '''
    char_list = []
    case_list = []
    toks = one_doc['toks']
    for tok in toks:
        char_list.append([c for c in tok])
        case_list.append(getCasing(tok))
    one_doc['chars'] = char_list
    # add flat chars representation
    one_doc['all_chars'] = [item for sublist in char_list for item in sublist]
    one_doc['cases'] = case_list

    return one_doc

#################################################################
# word2vec functions
#################################################################
def cbowWorker(one_doc, text_key, context_size, word2Idx, tokenIdentities):
    '''Assuming your text is CLEAN this will generate a continuous bag of words representation for you'''
    toks = one_doc[text_key]
    data = []
    data_idx = []
    for i in range(2, len(toks) - 2):
        # text data
        # context = [text[i - 2], text[i - 1], text[i + 1], text[i + 2]]
        context = toks[i-context_size:i] + toks[i+1:i+context_size+1]
        target = toks[i]
        data.append((context, target))
        # convert to indices
        ctx_idx = [word2Idx.get(word, word2Idx['UNKNOWN_TOKEN']) for word in context]
        target_idx = word2Idx.get(target, word2Idx['UNKNOWN_TOKEN'])
        target_identity = tokenIdentities[target_idx]
        data_idx.append((ctx_idx, target_idx, target_identity))
    one_doc['cbow'] = data
    one_doc['cbow_idx'] = data_idx
    return one_doc


def skipgramWorker(one_doc, text_key, word2Idx):
    '''
    NOTE: Still a work in progress!!  Use with caution
    :param one_doc:
    :param text_key:
    :param word2Idx:
    :return:
    '''
    text = one_doc[text_key]
    data = []
    data_idx = []
    for i in range(2, len(text) - 2):
        data.append((text[i], text[i-2], 1))
        data_idx.append((word2Idx[text[i]], word2Idx[text[i-2]], 1))

        data.append((text[i], text[i-1], 1))
        data_idx.append((word2Idx[text[i]], word2Idx[text[i-1]], 1))

        data.append((text[i], text[i+1], 1))
        data_idx.append((word2Idx[text[i]], word2Idx[text[i+1]], 1))

        data.append((text[i], text[i+2], 1))
        data_idx.append((word2Idx[text[i]], word2Idx[text[i+2]], 1))
        # negative sampling
        for _ in range(4):
            if random.random() < 0.5 or i >= len(text) - 3:
                rand_id = random.randint(0, i-1)
            else:
                rand_id = random.randint(i+3, len(text)-1)
            data.append((text[i], text[rand_id], 0))
            data_idx.append((word2Idx[text[i]], word2Idx[text[rand_id]], 0))
    one_doc['cbow'] = data
    one_doc['cbow_idx'] = data_idx
    return one_doc


def get_context(words, idx, window_size=5, use_random_offset=True):
    ''' Get a list of words in a window around an index. '''
    if use_random_offset:
        offset = np.random.randint(1, window_size + 1)
    else:
        offset = window_size
    start = max(idx - offset, 0)
    stop = idx + offset
    target_words = words[start:idx] + words[idx + 1:stop + 1]
    return list(target_words)
####################################################################
# Random
####################################################################
def take(n, iterable):
    "Return first n items of the iterable as a list"
    return list(islice(iterable, n))


def stack_pad(it, row_len=None, dtype='int32'):
    '''
    from seqence of sequences that may be variable length
    create a sequence of sequences all of the same length

    Args
        :row_len - if none, pads to length of longest sequence, else pads to length = row_len
        :dtype - the desired datatype for the resulting array
    '''
    def resize(row, size):
        new = np.array(row, dtype=dtype)
        new.resize(size)
        return new

    if row_len:
        row_length = row_len
    else:
        row_length = max(it, key=len).__len__()
    mat = np.array([resize(row, row_length) for row in it])

    return mat

def preprocess(text):
    # Replace punctuation with tokens so we can use them in our model
    text = text.lower()
    text = text.replace('.', ' <PERIOD> ')
    text = text.replace(',', ' <COMMA> ')
    text = text.replace('"', ' <QUOTATION_MARK> ')
    text = text.replace(';', ' <SEMICOLON> ')
    text = text.replace('!', ' <EXCLAMATION_MARK> ')
    text = text.replace('?', ' <QUESTION_MARK> ')
    text = text.replace('(', ' <LEFT_PAREN> ')
    text = text.replace(')', ' <RIGHT_PAREN> ')
    text = text.replace('--', ' <HYPHENS> ')
    text = text.replace('?', ' <QUESTION_MARK> ')
    # text = text.replace('\n', ' <NEW_LINE> ')
    text = text.replace(':', ' <COLON> ')
    words = text.split()

    # Remove all words with  5 or fewer occurences
    word_counts = Counter(words)
    trimmed_words = [word for word in words if word_counts[word] > 5]

    return trimmed_words


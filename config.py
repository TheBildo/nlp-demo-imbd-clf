"""
File to contain ML configuration options in dictionaries
Can be imported by both scripts and notebooks
"""

model_config = {
    'model_name': 'IMDB_Lstm_Dot_Attn_',
    # data loader params
    'data_file_path': '/data/example_text/doc_classification/aclImdb/',
    'zero_label':'negative',
    'doc_limit':200,
    'data_key':'batch_toks',
    'data_len_key':'batch_tok_lens',
    'label_key':'batch_labels',
    # model params    
    # 'model_device' : 'cpu',  # or cuda but only if you are on a GPU equipped and configured system
    'model_device' : 'cuda',  # or cuda but only if you are on a GPU equipped and configured system
    # embedding layer
    'embed_dim' : 180,
    # lstm layer
    'bilstm_flag': True,
    'layers':2,
    'hidden_dim' : 400,
    'dropout':0.1,

    # optimizer
    'optim':'sdg',
    'lr':0.01,
    'weight_decay':1e-6,

    # training params
    'epochs':3,

    'train_batch_size':32,
    'sort_train_docs': False,
    'shuffle_train_docs': True,

    'test_batch_size':32,
    'sort_test_docs': False,
    'shuffle_test_docs': False
    
}

FROM pytorch/pytorch:1.12.1-cuda11.3-cudnn8-runtime


# [Optional] Uncomment this section to install additional OS packages.
ENV DEBIAN_FRONTEND=noninteractive

# install networking tools
RUN apt-get update && apt-get install -y --no-install-recommends net-tools iputils-ping curl htop

RUN conda install ipykernel pandas matplotlib seaborn plotly nbformat nltk scikit-learn

# install NLTK data
RUN python -m nltk.downloader -d /usr/local/share/nltk_data all

# [Optional] Uncomment this line to install global node packages.
# RUN su vscode -c "source /usr/local/share/nvm/nvm.sh && npm install -g <your-package-here>" 2>&1

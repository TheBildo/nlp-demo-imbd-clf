import torch
import torch.nn as nn
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence
import torch.nn.functional as F


class LstmDotAttn(nn.Module):
    def __init__(self, config):
        super(LstmDotAttn, self).__init__()
        '''
        Implements LSTM with dot attention for text classification
        '''
        # Save config in model
        self.config = config

        ###########################################################
        # Model layer definitions
        ###########################################################
        # defining the word embeddings
        self.word_embeddings = nn.Embedding(self.config['vocab_size'], self.config['embed_dim'])

        # Defining the LSTM encoder
        if(self.config['bilstm_flag']):
            self.lstm_word = nn.LSTM(self.config['embed_dim'],
                                     int(self.config['hidden_dim']/2),
                                     num_layers=self.config['layers'],
                                     bidirectional=True,
                                     batch_first=True,
                                     dropout=self.config['dropout'])
        else:
            self.lstm_word = nn.LSTM(self.config['embed_dim'],
                                     self.config['hidden_dim'],
                                     num_layers=self.config['layers'],
                                     bidirectional=False,
                                     batch_first=True,
                                     dropout=self.config['dropout'])

        self.attn_relevance = DotAttentionLayer(self.config['hidden_dim'], self.config['model_device'])

        self.hidden2label = nn.Linear(self.config['hidden_dim'], self.config['label_size'])

    def init_hidden(self, bilstm_flag, batch_size, layers, hidden_dim, model_device):
        # num_layes, minibatch size, hidden_dim
        if(bilstm_flag):
            return (torch.FloatTensor(layers*2,
                                      batch_size,
                                      int(hidden_dim/2)).fill_(0).to(device=model_device),
                    torch.FloatTensor(layers*2,
                                      batch_size,
                                      int(hidden_dim/2)).fill_(0).to(device=model_device))
        else:
            return (torch.FloatTensor(layers,
                                      batch_size,
                                      hidden_dim).fill_(0).to(device=model_device),
                    torch.FloatTensor(layers,
                                      batch_size,
                                      hidden_dim).fill_(0).to(device=model_device))

    def forward(self, batch_inds, batch_lens):
        # Getting the embeddings
        embeds = self.word_embeddings(batch_inds)

        # Pack the whole batch and initialize the hidden and cell states for the LSTM encoder
        packed = pack_padded_sequence(embeds, batch_lens.cpu(), batch_first = True, enforce_sorted=False)
        self.hidden_vals = self.init_hidden(self.config['bilstm_flag'], embeds.shape[0], self.config['layers'], self.config['hidden_dim'], self.config['model_device'])

        # Getting the hidden states from the encoder for each of the token
        packed_output, self.hidden_vals = self.lstm_word(packed, self.hidden_vals)
        lstm_out = pad_packed_sequence(packed_output, batch_first=True)[0]

        # Getting sequence representation by doing weighted summation
        pad_op, alphas = self.attn_relevance((lstm_out, batch_lens))

        # Softmax probs for relevance classification
        tag_space_label = self.hidden2label(pad_op)
        tag_space_label = F.log_softmax(tag_space_label, dim=1)

        return tag_space_label, alphas


class DotAttentionLayer(nn.Module):
    def __init__(self, hidden_size, model_device):
        super(DotAttentionLayer, self).__init__()
        self.hidden_size = hidden_size
        self.model_device = model_device
        self.W = nn.Linear(hidden_size, 1, bias=False)

    def forward(self, input):
        """
        input: (unpacked_padded_output: batch_size x seq_len x hidden_size, lengths: batch_size)
        """
        inputs, lengths = input
        batch_size, max_len, _ = inputs.size()
        flat_input = inputs.contiguous().view(-1, self.hidden_size)
        logits = self.W(flat_input).view(batch_size, max_len)
        alphas = F.softmax(logits, dim=1)

        # computing mask
        idxes = torch.arange(0, max_len, out=torch.LongTensor(max_len).to(device=self.model_device), device=self.model_device).unsqueeze(0)
        mask = (idxes < lengths.unsqueeze(1)).float()
        alphas = alphas * mask

        # renormalize
        alphas = alphas / torch.sum(alphas, 1).view(-1, 1)
        output = torch.bmm(alphas.unsqueeze(1), inputs).squeeze(1)
        return output, alphas

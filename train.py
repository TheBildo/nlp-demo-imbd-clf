'''
    Script to train and evaluate the model
'''
import logging
import argparse

import torch
import torch.nn as nn
from sklearn.metrics import accuracy_score
from torch.utils import data
from tqdm import trange

from torch.utils.data import DataLoader
from tqdm.std import tqdm

# sibling files
from model import LstmDotAttn
from data_manager import (ImdbMovieReviewDataReader,
                        IMDBDataset,
                        PadCollateForTorch)
import config

######################################################################
# logger
######################################################################
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

# Uncomment if you want to add a file logger
# fh = logging.FileHandler('neuralarch_pt_clf.log')
# fh.setFormatter(formatter)
# fh.setLevel(logging.DEBUG)
# logger.addHandler(fh)


######################################################################
# classes
######################################################################
class RunningAverage():
    """A simple class that maintains the running average of a quantity

    Example:
    ```
    loss_avg = RunningAverage()
    loss_avg.update(2)
    loss_avg.update(4)
    loss_avg() = 3
    ```
    """

    def __init__(self):
        self.steps = 0
        self.total = 0

    def update(self, val):
        self.total += val
        self.steps += 1

    def __call__(self):
        return self.total / float(self.steps)


######################################################################
# function defs
######################################################################
def evaluate(model, data_iterator, data_key, data_len_key, label_key):
    model.eval()
    # set up some holders
    pred_label = []
    actual_label = []
    ret_dict = {}

    pbatches = tqdm(data_iterator)
    for batch_dict in pbatches:
        # run the model
        tag_label, alphas = model(batch_dict[data_key], batch_dict[data_len_key])
        ##################################################################
        # compute metrics
        ##################################################################
        act_label_val, act_label_ind = torch.max(batch_dict[label_key], dim=1)
        actual_label += act_label_ind.tolist()
        # argmax of predictions
        tag_label_val, tag_label_inds = torch.max(tag_label, dim=1)
        pred_label += tag_label_inds.tolist()

    # mean the batch level metrics here and report them
    acc = accuracy_score(actual_label, pred_label)

    logger.info(f'Accuracy {acc}')

    ret_dict['actual_label'] = actual_label
    ret_dict['pred_label'] = pred_label
    return ret_dict


def train(model, loss_function, optimizer, data_iterator, data_key, data_len_key, label_key):
    """
    Trains the model for one epoch, aka one complete pass over the data
    """
    model.train()

    # set up some holders
    pred_label = []
    actual_label = []

    tot_train_loss = 0
    loss_avg = RunningAverage()

    pbatches = tqdm(data_iterator)
    for batch_dict in pbatches:
        # run the model
        tag_label, alphas = model(batch_dict[data_key], batch_dict[data_len_key])

        ##################################################################
        # compute loss
        ##################################################################
        act_label_val, act_label_ind = torch.max(batch_dict[label_key], dim=1)
        actual_label += act_label_ind.tolist()
        # argmax of predictions
        tag_label_val, tag_label_inds = torch.max(tag_label, dim=1)
        pred_label += tag_label_inds.tolist()

        loss = torch.sum(loss_function(tag_label, act_label_ind))

        loss_avg.update(loss)
        tot_train_loss += loss.item()

        # clear previous gradients, compute gradients of all variables wrt loss
        optimizer.zero_grad()
        loss.backward()
        # performs updates using calculated gradients
        optimizer.step()

        pbatches.set_postfix(loss='{:05.3f}'.format(loss))

    logger.info(f'Total Train Loss = {tot_train_loss}')
    logger.info(f'Loss Avg = {loss_avg()}')


def run_epochs(model, loss_function, optimizer,train_docs,test_docs,
               model_config):

    logger.info("Building Data Loaders")
    imdb_train_dataset = IMDBDataset(train_docs, sort_by_len=model_config['sort_train_docs'])
    imdb_train_dl = DataLoader(imdb_train_dataset,
                 batch_size = model_config['train_batch_size'],
                collate_fn=PadCollateForTorch(padding_token=model_config['padding_token'],
                                              num_labels=model_config['label_size'],
                                              torch_device=model_config['model_device'],
                                              torch_dtype=torch.long),
                shuffle=model_config['shuffle_train_docs'])


    imdb_test_dataset = IMDBDataset(test_docs, sort_by_len=model_config['sort_test_docs'])
    imdb_test_dl = DataLoader(imdb_test_dataset,
                 batch_size = model_config['test_batch_size'],
                 collate_fn=PadCollateForTorch(padding_token=model_config['padding_token'],
                                              num_labels=model_config['label_size'],
                                              torch_device=model_config['model_device'],
                                              torch_dtype=torch.long),
                shuffle=model_config['shuffle_test_docs'])

    epochs = model_config['epochs']
    for epoch in range(epochs):
        logger.info(f'======================= Now Running Epoch {epoch+1}/{epochs}! ==============================')
        logger.info('Training')
        train(model, loss_function, optimizer, imdb_train_dl, model_config['data_key'], model_config['data_len_key'], model_config['label_key'])

        logger.info('Evaluating')
        # eval on test set
        # determine batch_size
        _ = evaluate(model, imdb_test_dl, model_config['data_key'], model_config['data_len_key'], model_config['label_key'])


######################################################################
# main
######################################################################
def main():
    logger.info("Hello, I'm going to train a binary classifier on IMDB data for you")

    model_config = config.model_config
    logger.info(model_config)

    # load data (passing device)
    imdb_data_reader = ImdbMovieReviewDataReader(logger,
                                            model_config['data_file_path'],
                                            load_limit=model_config['doc_limit'])
    train_docs, test_docs = imdb_data_reader.load_docs()

    # vectorize the text - aka make texty things numbery things
    train_docs = imdb_data_reader.pre_process_docs(train_docs, build_index_maps=True)
    test_docs = imdb_data_reader.pre_process_docs(test_docs, build_index_maps=False)
    model_config['vocab_size'] = len(imdb_data_reader.tok2Idx)
    model_config['label_size'] = len(imdb_data_reader.label2Idx)
    model_config['padding_token'] = imdb_data_reader.tok2Idx['PADDING_TOKEN']


    if model_config['model_device'] == 'cpu':
        model = LstmDotAttn(model_config)
    elif model_config['model_device'] == 'cuda' and torch.cuda.is_available():
        model = LstmDotAttn(model_config).cuda()
    else:
        print('Incompatiable configuration')
    logger.info(model)
    # add loss and optimizer
    loss_function = nn.NLLLoss(reduction='none')
    optimizer = torch.optim.Adam(model.parameters())

    # train and eval
    run_epochs(model,
               loss_function,
               optimizer,
               train_docs,
               test_docs,
               model_config)


if __name__ == '__main__':
    main()

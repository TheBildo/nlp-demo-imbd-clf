# Text Classification with Deep Learning using IMDB data
A working example of a text classifier using word embeddings, LSTM RNN layer, and attention to classify text documents.  Most documentation/comments/explanation is contained within the jupyter notebooks.  This document will help you build the python virtual environment (using Docker) that configures a container with all the necessary pre-reqs to run the code.

> NOTE: There is an associated dataset required to run this code.  Please see the 'Text Classifcation Example...ipynb' notebook for a link as well as details about the optimal directory structure.  Since this project is using Docker, the data will live on your host OS and during the Docker container startup will be mounted into the container so that it is available inside your python environemnt.  You may need to slightly alter the "mounts" line in .devcontainer/devcontainer.json depending on where you saved the data on your machine.

## Codebase Quickstart
This section will help you get started with the code

### Pre-Reqs

Download and install:

1. Docker - https://www.docker.com 
1. VSCode - https://code.visualstudio.com ; Then using the extensions manager search for and install these VSC extensions
    - remote development extension pack
    - python
    - pylance
    - jupyter


1. Pull the code from git.  The directory created during the pull will become your <project_directory>
1. Open <project_directory> in VSCode
1. Use the 'Dev Container' exenstion to 'Rebuild and reopen in container'
    - This uses the Dockerfile in conjunction with .devcontainer/devcontainer.json to build an execution environment into a Docker container for you.  It then connects VSCode to it providing a fully integrated development environment
1. Run the tutorials by opening and executing code in either of the 2 notebooks
    - VSCode provides a fully baked Jupyter environment for you directly within the IDE

> NOTE: You may need to edit lines 34 and 37 of the devcontainer.json file.  Line 34 should be deleted or commented out if your system is not configured for use with CUDA architectured GPU's.  Line 37 may need to be edited depending on where you save the dataset on your host machine (mentioned above)

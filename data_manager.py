'''
    Class to encapsulate data loading in the cnext path text experiment
    Assume tokenization is controlled at data loader level - project specific
    After tokenization, nlp utils centralizes all common tasks
    Make this part of pre-process docs
'''
import os
import glob
import numpy as np
import torch
from torch.utils.data import Dataset
import random
from nltk.tokenize import word_tokenize
from multiprocessing import Pool, cpu_count

# helpers
from nlp_utils_v2 import (createLabelToNumIds,
                            createTokToNumIds,
                            convertTokToNumbersWorker,
                            convertLabelToNumberWorker)


class ImdbMovieReviewDataReader(object):
    """Cnext path text data set """

    def __init__(self, logger, data_file_path,
                 load_limit=500,
                 use_pretrained_ebmeddings=False,
                 pre_trained_embeddings_file_path = None,
                 char_pad_len=52,
                 train_size=0.8):
        self.data_file_path = data_file_path
        self.load_limit = load_limit
        self.train_size = train_size
        self.logger = logger
        self.use_pretrained_ebmeddings = use_pretrained_ebmeddings
        self.pre_trained_embeddings_file_path = pre_trained_embeddings_file_path
        # set to 52 since no single word is likely to ever exceed this length
        self.charPadLen = char_pad_len

        # constants
        self.pos_label='positive'
        self.neg_label='negative'

    def load_docs(self, shuffle_on_load = False):
        '''
        Reads docs from persistence location, in this case csv file on disk into
        a list of dictionary objects, each containing:
        1. id - line number
        1. doc_str - string representing the document
        1. label - human readable
        '''
        self.logger.info('Loading Documents')
        ##########################
        # Train Docs
        ##########################
        positive_path = os.path.join(self.data_file_path, 'train', 'pos')
        negative_path = os.path.join(self.data_file_path, 'train', 'neg')
        train_docs = []
        count = 0
        for filename in glob.glob(os.path.join(positive_path, '*.txt')):
            with open(filename, 'r') as f:
                one_doc_str = f.read()
                train_docs.append({'id': count,
                                   'doc_str': one_doc_str,
                                   'label': self.pos_label})
                count += 1
            if count >= self.load_limit:
                # stop loading
                break

        count = 0
        for filename in glob.glob(os.path.join(negative_path, '*.txt')):
            with open(filename, 'r') as f:
                one_doc_str = f.read()
                train_docs.append({'id': count,
                                   'doc_str': one_doc_str,
                                   'label': self.neg_label})
                count += 1
            if count >= self.load_limit:
                # stop loading
                break
        # shuffling the docs
        if shuffle_on_load:
            random.shuffle(train_docs)
        self.logger.info('Train Doc Count = ')
        self.logger.info(len(train_docs))

        ###########################
        # Test Docs
        ###########################
        positive_path = os.path.join(self.data_file_path,'test', 'pos')
        negative_path = os.path.join(self.data_file_path,'test', 'neg')
        test_docs = []
        count = 0
        for filename in glob.glob(os.path.join(positive_path, '*.txt')):
            with open(filename, 'r') as f:
                one_doc_str = f.read()
                test_docs.append({'id': count,
                                  'doc_str': one_doc_str,
                                  'label': self.pos_label})
                count += 1
            if count >= self.load_limit:
                # stop loading
                break

        count = 0
        for filename in glob.glob(os.path.join(negative_path, '*.txt')):
            with open(filename, 'r') as f:
                one_doc_str = f.read()
                test_docs.append({'id': count,
                                  'doc_str': one_doc_str,
                                  'label': self.neg_label})
                count += 1
            if count >= self.load_limit:
                # stop loading
                break

        if shuffle_on_load:
            random.shuffle(test_docs)
        self.logger.info('Test Doc Count = ')
        self.logger.info(len(test_docs))

        return train_docs, test_docs



    def pre_process_docs(self, docs, train_data=True, build_index_maps=False, zero_label='negative'):
        '''
        Assumes text has been tokenized prior to calling the functions in nlp utils
        Using nlp_utils functions, convert text to numbers
        '''
        self.logger.info('Preprocessing Documents')
        self.logger.info('Tokenizing')
        with Pool(cpu_count()) as p:
            docs = p.map(tokenizer_worker, docs)

        with Pool(cpu_count()) as p:
            tok_len_lists = p.map(token_characterization_worker, docs)
        all_tok_len_array = np.array([item for sublist in tok_len_lists for item in sublist])
        all_len_list = np.array([len(sublist) for sublist in tok_len_lists])

        self.min_tok_len = all_tok_len_array.min()
        self.mean_tok_len = all_tok_len_array.mean()
        self.max_tok_len = all_tok_len_array.max()

        self.min_doc_tok_count = all_len_list.min()
        self.mean_doc_tok_count = all_len_list.mean()
        self.max_doc_tok_count = all_len_list.max()

        # Build dictionaries that map from words to numbers and back again
        # these get saved in the data_loader class
        if build_index_maps:
            self.logger.info('Building index maps')
            (self.tok2Idx,
             self.idx2Tok,
             self.tokCounts,
             self.tokIdentities) = createTokToNumIds(docs)

            (self.label2Idx,
             self.idx2Label,
             self.labelIdentities) = createLabelToNumIds(docs,
                                                         label_key='label',
                                                         zero_label=zero_label)

        self.logger.info('Converting texty things to Numbery things')
        # tok2Idx, case2Idx, char2Idx, charPadLen
        with Pool(cpu_count()) as p:
            docs = p.starmap(convertTokToNumbersWorker, [(doc, self.tok2Idx) for doc in docs])

        if train_data:
            self.logger.info('Converting labelly things to numbery things')
            with Pool(cpu_count()) as p:
                # text_label_key, idx_label_key, label2Idx, labelIdentities
                docs = p.starmap(convertLabelToNumberWorker, [(doc, 'label', 'label_idx',
                                                               self.label2Idx, self.labelIdentities) for doc in docs])

        return docs

    def data_iterator(self, docs, batch_size, torch_device='cpu', torch_dtype=torch.int32, sort_by_len=True, shuffle=False):
        """
        Returns a generator that yields batches data with labels.
        Expires after one pass over the data.

        Args:
            docs: A list with structure [[tokens],[case],[[chars],[chars]],[labels]]
            batch_size: How many examples should be in each batch
            shuffle: (bool) whether the data should be shuffled

        Yields:
            batch_data: (Variable) dimension batch_size x seq_len with the sentence data
            batch_sent_labels: (Variable) dimension batch_size x seq_len with the corresponding hist labels
            test_docs: (Variable) dimension batch_size x seq_len with the corresponding site labels

        """
        if sort_by_len:
            docs = sorted(docs, key=lambda x: len(x['toks_idx']))
        # define order of examples for this iteration
        order = list(range(len(docs)))
        if shuffle:
            # shuffle the order if requested
            random.shuffle(order)

        # one pass over data
        for i in range((len(docs)+1)//batch_size):
            # fetch sentences and tags
            batch_docs = [docs[idx] for idx in order[i*batch_size:(i+1) * batch_size]]
            # tokens are at index 0 - this gets the longest document in the mix
            batch_max_len = max([len(x['toks_idx']) for x in batch_docs])

            # create emtpy arrays equal to max len
            batch_tokens = self.tok2Idx['PADDING_TOKEN']*np.ones((len(batch_docs), batch_max_len), dtype='int32')
            batch_sent_labels = np.zeros((len(batch_docs), len(self.label2Idx)), dtype='int32')
            batch_lens = np.zeros(len(batch_docs), dtype='int32')

            for j in range(len(batch_docs)):
                one_doc = batch_docs[j]
                cur_len = len(one_doc['toks_idx'])
                batch_tokens[j][:cur_len] = one_doc['toks_idx']
                batch_lens[j] = cur_len
                
                batch_sent_labels[j] = one_doc['label_idx']

            # make sure all variables are a numpy array before torch.from_numpy'ing them to tensors
            assert isinstance(batch_tokens, np.ndarray)
            assert isinstance(batch_lens, np.ndarray)
            assert isinstance(batch_sent_labels, np.ndarray)

            # convert to torch objects with optional cuda TBD
            batch_tokens = torch.from_numpy(batch_tokens).to(device=torch_device, dtype=torch_dtype)
            batch_lens = torch.from_numpy(batch_lens).to(device=torch_device, dtype=torch_dtype)
            batch_sent_labels = torch.from_numpy(batch_sent_labels).to(device=torch_device, dtype=torch_dtype)

            yield batch_tokens, batch_lens, batch_sent_labels

class IMDBDataset(Dataset):
    def __init__(self, docs,
                 sort_by_len=False,
                 shuffle_docs=False):
        self.docs = docs
        if sort_by_len:
            self.docs = sorted(self.docs, key=lambda x: len(x['toks_idx']))
        if shuffle_docs:
            shuffle(self.docs)

    def __len__(self):
        return len(self.docs)

    def __getitem__(self, item):
        return self.docs[item]


class PadCollateForTorch(object):
    def __init__(self, padding_token=0, num_labels=2, torch_device='cpu', torch_dtype=torch.int32,
                 train_data=True,
                 include_id=False):
        self.padding_token = padding_token
        self.num_labels = num_labels
        self.torch_device = torch_device
        self.torch_dtype = torch_dtype
        self.train_data = train_data
        self.include_id = include_id

    def __call__(self, batch):
        return self.pad_collate(batch)

    def pad_collate(self, batch):
        # batch is a list of dictionaries
        # We need to pad one dict entry to the max len of that entry for that batch
        # find the max len
        batch_max_len = max([len(x['toks_idx']) for x in batch])
        # create emtpy arrays equal to max len
        batch_toks = self.padding_token * np.ones((len(batch), batch_max_len), dtype='int32')
        batch_tok_lens = np.zeros(len(batch), dtype='int32')
        if self.train_data:
            batch_labels = np.zeros((len(batch), self.num_labels), dtype='int32')
        if self.include_id:
            batch_ids = []

        # iterate through batch and convert things to tensors, only retaining the items
        # required for training.  In this case the tok_idx and the labels
        for j in range(len(batch)):
            one_doc = batch[j]
            cur_len = len(one_doc['toks_idx'])
            batch_toks[j][:cur_len] = one_doc['toks_idx']
            batch_tok_lens[j] = cur_len
            if self.train_data:
                batch_labels[j] = one_doc['label_idx']
            if self.include_id:
                batch_ids.append(one_doc['id'])

        # make sure all variables are a numpy array before torch.from_numpy'ing them to tensors
        assert isinstance(batch_toks, np.ndarray)
        assert isinstance(batch_tok_lens, np.ndarray)
        if self.train_data:
            assert isinstance(batch_labels, np.ndarray)

        # convert to torch objects with optional cuda TBD
        batch_toks = torch.from_numpy(batch_toks).to(device=self.torch_device, dtype=self.torch_dtype)
        batch_tok_lens = torch.from_numpy(batch_tok_lens).to(device=self.torch_device, dtype=self.torch_dtype)
        # labels
        if self.train_data:
            batch_labels = torch.from_numpy(batch_labels).to(device=self.torch_device, dtype=self.torch_dtype)

            ret_dict = {'batch_toks': batch_toks,
                        'batch_tok_lens': batch_tok_lens,
                        'batch_labels': batch_labels}
        else:
            ret_dict = {'batch_toks': batch_toks,
                        'batch_tok_lens': batch_tok_lens}

        if self.include_id:
            ret_dict['batch_ids'] = batch_ids

        return ret_dict





############################################################################
# Non-class functions
############################################################################
def tokenizer_worker(one_doc):
    str_to_tokenize = one_doc['doc_str']
    one_doc['toks'] = word_tokenize(str_to_tokenize)
    return one_doc

def token_characterization_worker(one_doc):
    one_doc_toks = one_doc['toks']
    return [len(x) for x in one_doc_toks]

